# README #

1/3スケールのJR100風小物のstlファイルです。 
スイッチ類、I/Oポート等は省略しています。 
 
組み合わせたファイルとパーツごとに配置したファイルの二種類があります。 
元ファイルはAUTODESK 123D DESIGNです。 


***

# 実機情報

## メーカ
- Nationnal
- 松下電器産業

## 発売時期
- JR-100 1981年11月21日

## 参考資料

- [Wikipedia](https://ja.wikipedia.org/wiki/JR_(%E3%82%B3%E3%83%B3%E3%83%94%E3%83%A5%E3%83%BC%E3%82%BF))
- [懐かしのホビーパソコン紹介](https://twitter.com/i/events/888359829512699906)

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_jr100/raw/99662fdf9deb9e2724374dedcca5d0c5a45981fd/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_jr100/raw/99662fdf9deb9e2724374dedcca5d0c5a45981fd/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_jr100/raw/99662fdf9deb9e2724374dedcca5d0c5a45981fd/ExampleImage.jpg)
